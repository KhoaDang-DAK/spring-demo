FROM maven:ibmjava-alpine as java

COPY . /app

WORKDIR /app

RUN mvn clean package

###########

FROM openjdk:8-alpine

WORKDIR /app

COPY --from=java /app/target/websocket-demo-0.0.1-SNAPSHOT.jar /app

CMD ["java" , "-Djava.security.egd=file:/dev/./urandom" , "-jar" , "websocket-demo-0.0.1-SNAPSHOT.jar"]


